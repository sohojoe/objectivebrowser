//
// Created by leeg on 07/05/2012.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <clang-c/Index.h>
#import "FZAModelBuildingParserDelegate.h"
#import "FZAClassGroup.h"
#import "FZAClassDefinition.h"
#import "FZAMethodDefinition.h"
#import "FZAPropertyDefinition.h"
#import "FZASourceDefinition.h"
#import "FZASourceFile.h"


@implementation FZAModelBuildingParserDelegate {
    FZAClassGroup *group;
    FZAClassDefinition *currentClass;
    NSData *fileContent;
    NSString *fileName;
}

- (id)initWithClassGroup:(FZAClassGroup *)classGroup {
    if ((self = [super init])) {
        group = classGroup;
    }
    return self;
}

- (void)classParser: (FZAClassParser *)parser willBeginParsingFile: (NSString *)path {
    fileContent = [NSData dataWithContentsOfMappedFile: path];
    fileName = [path lastPathComponent];
}

- (void)classParser:(FZAClassParser *)parser didFinishParsingFile:(NSString *)path {
    fileContent = nil;
    fileName = nil;
}

- (void)classParser:(FZAClassParser *)parser foundDeclaration:(CXIdxDeclInfo const *)declaration {
    const char * const name = declaration->entityInfo->name;
    if (name == NULL) return; //not much we could do anyway.
    NSString *declarationName = [NSString stringWithUTF8String: name];

    switch (declaration->entityInfo->kind) {
        case CXIdxEntity_ObjCProtocol:
        {
            currentClass = nil;
            break;
        }
        case CXIdxEntity_ObjCCategory:
        {
            const CXIdxObjCCategoryDeclInfo *categoryInfo = 
            clang_index_getObjCCategoryDeclInfo(declaration);
            NSString *className = [NSString stringWithUTF8String: categoryInfo->objcClass->name];
            FZAClassDefinition *classDefinition =[group classNamed: className];
            if (!classDefinition) {
                classDefinition = [[FZAClassDefinition alloc] init];
                classDefinition.name = className;
                [group insertObject: classDefinition inClassesAtIndex: [group countOfClasses]];
            }
            currentClass = classDefinition;
            break;
        }
        case CXIdxEntity_ObjCClass:
        {
            FZAClassDefinition *classDefinition =[group classNamed: declarationName];
            if (!classDefinition) {
                classDefinition = [[FZAClassDefinition alloc] init];
                classDefinition.name = declarationName;
                [group insertObject: classDefinition inClassesAtIndex: [group countOfClasses]];
            }
            currentClass = classDefinition;
            break;
        }
        case CXIdxEntity_ObjCClassMethod:
        case CXIdxEntity_ObjCInstanceMethod:
        {
            if (declaration->isImplicit) {
                break;
            }
            FZAMethodDefinition *method = [[FZAMethodDefinition alloc] init];
            method.selector = declarationName;
            if (declaration->entityInfo->kind == CXIdxEntity_ObjCClassMethod)
                method.type = FZAMethodClass;
            else
                method.type = FZAMethodInstance;
            if (declaration->isDefinition) {
                CXSourceRange range = clang_getCursorExtent(declaration->cursor);
                CXSourceLocation start = clang_getRangeStart(range);
                CXSourceLocation end = clang_getRangeEnd(range);
                unsigned startOffset, endOffset;
                clang_getExpansionLocation(start, NULL, NULL, NULL, &startOffset);
                clang_getExpansionLocation(end, NULL, NULL, NULL, &endOffset);
                NSRange sourceRange = NSMakeRange(startOffset, endOffset - startOffset);
                NSData *definition = [fileContent subdataWithRange: sourceRange];
                NSString *definitionCode = [[NSString alloc] initWithData: definition encoding: NSUTF8StringEncoding];
                FZASourceDefinition *source = [[FZASourceDefinition alloc] init];
                source.file = fileName;
                source.range = sourceRange;
                source.definition = definitionCode;
                method.sourceCode = source;
            }
            [currentClass insertObject: method inMethodsAtIndex: [currentClass countOfMethods]];
            break;
        }
        case CXIdxEntity_ObjCProperty:
        {
            const CXIdxObjCPropertyDeclInfo *propertyDeclaration = clang_index_getObjCPropertyDeclInfo(declaration);
            FZAPropertyDefinition *property = [[FZAPropertyDefinition alloc] init];
            property.title = declarationName;
            if (propertyDeclaration && propertyDeclaration->setter) {
                property.access = FZAPropertyAccessReadWrite;
            } else {
                property.access = FZAPropertyAccessReadOnly;
            }
            [currentClass insertObject: property inPropertiesAtIndex: [currentClass countOfProperties]];
            break;
        }
        default:
            break;
    }
}

- (CXIdxClientFile)classParser:(FZAClassParser *)parser enteredMainFile:(CXFile)mainFile {
    FZASourceFile *file = [[FZASourceFile alloc] init];
    file.frameworkFile = NO;
    const char *mainPath = clang_getCString(clang_getFileName(mainFile));
    file.path = [[NSFileManager defaultManager] stringWithFileSystemRepresentation: mainPath length: strlen(mainPath)];
    [group insertObject: file inFilesAtIndex: [group countOfFiles]];
    return (__bridge CXIdxClientFile)file;
}

- (CXIdxClientFile)classParser:(FZAClassParser *)parser includedFile:(const CXIdxIncludedFileInfo *)includedFile {
    FZASourceFile *file = [[FZASourceFile alloc] init];
    file.frameworkFile = includedFile->isAngled;
    const char *path = clang_getCString(clang_getFileName(includedFile->file));
    file.path = [[NSFileManager defaultManager] stringWithFileSystemRepresentation: path length: strlen(path)];
    [group insertObject: file inFilesAtIndex: [group countOfFiles]];
    return (__bridge CXIdxClientFile)file;
}

@end