//
//  FZASourceDeclaration.m
//  ObjectiveBrowser
//
//  Created by Graham Lee on 12/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FZASourceDefinition.h"

@implementation FZASourceDefinition

@synthesize file;
@synthesize range;
@synthesize definition;

@end
